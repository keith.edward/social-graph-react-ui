import React from 'react';
import { Chart } from 'regraph';
import {Route, Routes, Link} from "react-router-dom";

// TODO Create 2 folders in the features folder for each of the graphs created on this page then import them here.



import msTeamsData from './ms-teams-data.json';
import amountData from './amount-data.json';

function createNode(record, graphNodes) {
    const {user_id} = record.properties;
    if (!graphNodes[user_id]) {
        graphNodes[user_id] = {
            color: '#5fe3bf',
            label: { text: 'Anonymous User' }
        }
    }
}

function processRecord(record, graphNodes) {
    const {receiver, giver} = record;
    createNode(receiver, graphNodes);
    createNode(giver, graphNodes);
    const linkKey = `${receiver.properties.user_id}-${giver.properties.user_id}`
    if (graphNodes[linkKey]) {
        if (graphNodes[linkKey].total_amount)
        graphNodes[linkKey].width++
    } else {
        graphNodes[linkKey] = {
            id1: receiver.properties.user_id,
            id2: giver.properties.user_id,
            color: 'rgb(4, 129, 112)',
            width: 1,
            end1: {
                "arrow": true
            }
        }
    }
}

const graphNodes = {};
msTeamsData.forEach((record) => {
    processRecord(record, graphNodes)
});

const amountNodes = {};
amountData.forEach((record) => {
    processRecord(record, amountNodes);
});

function App() {
  return (
      <Routes>
          <Route path="/msteams" element={<Chart style={{ flex: 1, width: '100%', height: '100%' }} items={graphNodes}/>} />
          <Route path="/amount" element={<Chart style={{ flex: 1, width: '100%', height: '100%' }} items={amountNodes}/>} />
      </Routes>
  );
}

export default App;
